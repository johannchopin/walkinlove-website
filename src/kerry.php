<?php

	$trek_name = "Kerry Way";

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<link rel="stylesheet" type="text/css" href="les_treks/kerry.css">

		<?php include("les_treks/mainpage_head.php"); ?>

		<title>WalkInLove - Nos Treks</title>
	</head>
	<body style="margin: 0; padding: 0;">
		

		<header>
			<?php include("header/header_mini.php");?>			
		</header>

		<main>
			<?php include('go_up/go_up.php');?>

			<div class="map">
				<img alt="localisation Kerry Way" src="img/les_treks_img/kerry.png">
			</div>

			<div class="kerry_title">
				<h3>#4<br>- KERRY WAY (IRLANDE) -</h3>
			</div>

			<div>
				<div class="treks_intro">
					&nbsp;&nbsp;Ce magnifique circuit de randonnée de plus de <span class="important">200 km</span> démarre et s’achève à <span class="killarney">Killarney</span>, et permet de découvrir les beautés du <span class="ring_of_kerry">Ring of Kerry</span>, ainsi que la culture gaélique, toujours bien ancrée aujourd’hui. Les côtes déchiquetées irlandaises sont époustouflantes par leur beauté sauvage et leurs couleurs alternant camaïeux de verts et de bleus. Côté <span class="bivouac">bivouac</span>, vous aurez l’embarras du choix !
				</div>
			</div>

			<!-- INFOBULLE_TEXTE -->

			<div class="infobulle_container">	
				<div class="infobulle_killarney">
					<img alt="vue sur Killarney" src="img/les_treks_img/killarney.png">
					<div>
						<div class="infobulle_text">
							&nbsp;Killarney est une ville au bord du lac Lough Leane, dans le comté de Kerry, dans le sud-ouest de l'Irlande. Elle constitue une étape sur la route panoramique du Ring of Kerry, ainsi que le point de départ et d'arrivée du sentier de randonnée Kerry Way.
						</div>
					</div>
				</div>
				<div class="infobulle_ring_of_kerry">
					<img alt="vue sur le Kerry Ring" src="img/les_treks_img/ring_of_kerry.png">
					<div>
						<div class="infobulle_text">
							&nbsp;L'Anneau du Kerry est un circuit pittoresque qui fait le tour de la péninsule d'Iveragh dans le comté de Kerry, au sud-ouest de l'Irlande. Son parcours circulaire de 179 km comprend des paysages côtiers accidentés et verdoyants ainsi que des villages ruraux de bord de mer. 
						</div>
					</div>
				</div>
				<div class="infobulle_bivouac">
					<img alt="photo sur deux sacs" src="img/les_treks_img/bivouac.png">
					<div>
						<div class="infobulle_text">
							&nbsp;Côté bivouac, vous aurez l’embarras du choix, entre campings, B&amp;B locaux, et petits restaurants et Pubs ! De quoi passer 200 km incroyables !
						</div>
					</div>
				</div>
			</div>

			<!-- FIN_INFOBULLE_TEXTE -->
			
			
			<div class="carrousel_container">
				<div style="margin-bottom: 20px;">
					<div style="width: 50px;border-bottom: solid white 2px; margin: auto;"></div>
				</div>
				<div class="swiper-container_2">
				    <div class="swiper-wrapper">
				    	<div class="swiper-slide" id="swiper-slide_2" style="background-image:url(img/kerry_carrousel_1.png)"></div>
				    	<div class="swiper-slide" id="swiper-slide_2" style="background-image:url(img/kerry_carrousel_2.png)"></div>
				    </div>
				    <!-- Add Pagination -->
				    <div class="swiper-pagination swiper-pagination-white"></div>
				    <div class="swiper-button-next"></div>
				    <div class="swiper-button-prev"></div>
				</div>
				<div style="margin-top: 20px;">
					<div style="width: 50px;border-bottom: solid white 2px; margin: auto;"></div>
				</div>
			</div>


			<div class="titre_section">
				<div style="display: flex; width: auto;">
					<img alt="icon 'lieu'" src="img/lieu_icon.png" style="width: 50px; height: 60px;">
					<div style="display: flex; align-items: center;">
						<div>
							Lieu de départ
						</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
				<div class="destination">
					Kerry
				</div>
			</div>

			<div style="display: flex;">
				<div class="bordure">
					<img alt="icon d'avion" src="img/avion_icon.png">
					<div class="delimitation_avion"></div>
				</div>
				<div class="trajet_contain">
					<iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d5184232.391205259!2d-7.812057102897752!3d50.620364332297044!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x47e66e1f06e2b70f%3A0x40b82c3688c9460!2sParis!3m2!1d48.856614!2d2.3522219!4m5!1s0x48454fb8cce92d47%3A0x1800c7acf1981e00!2sKerry%2C+Irlande!3m2!1d52.1678937!2d-9.5259857!5e0!3m2!1sfr!2sfr!4v1515101511815" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					<div>
						<a href="https://www.airfrance.fr" target="blanc"><img alt="airfrance logo" src="img/airfrance_logo.png"></a>
						<a href="http://www.volotea.com/fr" target="blanc"><img alt="volotea logo" src="img/volotea_logo.png"></a>
						<a href="http://www.liligo.fr" target="blanc"><img alt="liligo logo" src="img/liligo_logo.png"></a>
						<a href="https://www.easyvoyage.com" target="blanc"><img alt="easyvoyage logo" src="img/easyvoyage_logo.png"></a>
					</div>
				</div>
			</div>

			<div class="titre_section">
				<div style="display: flex; margin: auto;">
					<img alt="icon séjour" src="img/sejour_icon.png">
					<div>
						<div>Déroulement du Trek</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
			</div>

			<div style="width: 100%; display: flex;">	
				<div class="bordure">
					<img alt="icon sac à dos" src="img/voyage_icon.png">
					<div class="delimitation_sejour"></div>
				</div>
				<div style="margin: auto; display: flex;" class="table">
					<table cellspacing="30" cellpadding="5">
						<thead>
							<tr>
								<th>Etapes</th>
								<th>Description</th>
								<th>Longueur</th>
								<th>Durée</th>
								<th>Niveau</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>#1</td>
								<td>Killarney - Kenmare</td>
								<td>15 km</td>
								<td>5 - 6 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#2</td>
								<td>Kenmare - Blackwater Bridge</td>
								<td>13 km</td>
								<td>5 heures</td>
								<td>**</td>
							</tr>
							<tr>
								<td>#3</td>
								<td>Blackwater Bridge - Sneem</td>
								<td>22 km</td>
								<td>9 - 10 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#4</td>
								<td>Sneem - Caherdaniel</td>
								<td>10 km</td>
								<td>5 - 6 heures</td>
								<td>*</td>
							</tr>
							<tr>
								<td>#5</td>
								<td>Caherdaniel - Waterville</td>
								<td>9 km</td>
								<td>4 - 5 heures</td>
								<td>**</td>
							</tr>
							<tr>
								<td>#6</td>
								<td>Waterville - Coars Crossroads</td>
								<td>17 km</td>
								<td>7 - 9 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#7</td>
								<td>Coars Crossroads - Foilmore</td>
								<td>9 km</td>
								<td>5 - 6 heures</td>
								<td>**</td>
							</tr>
							<tr>
								<td>#8</td>
								<td>Foilmore - Mountain Stage</td>
								<td>9 km</td>
								<td>7 - 8 heures</td>
								<td>****</td>
							</tr>
							<tr>
								<td>#9</td>
								<td>Mountain Stage - Glencar</td>
								<td>9 km</td>
								<td>5 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#9</td>
								<td>Glencar - Killarney</td>
								<td>12 km</td>
								<td>5 - 6 heures</td>
								<td>****</td>
							</tr>
						</tbody>
					</table>
					<div style="display: flex;">
						<img alt="marche Kerry Way map" src="img/les_treks_img/kerry_road.png">
					</div>
					</div>
			</div>

			<div class="titre_section">
				<div style="display: flex; margin: auto;">
					<img src="img/information2_icon.png" style="width: 50px; height: 50px;">
					<div>
						<div>Pour des informations supplémentaires</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
			</div>

			<div style="width: 100%; display: flex;">
				<div class="bordure">
					<img alt="icon information" src="img/information_icon.png">
					<div class="delimitation_information"></div>
				</div>
				<div class="information_contain">
					<div class="carte_adresse">
						Glencar Camping Kerry way<br>
						Maghanlawaun<br>
						Glencar Co<br>
						IRLANDE
						<div style="display: flex;">
							<a href="tel:+353669760215">
								<div class="telephone">
									+353 66 976 0215
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FKerry-Way%2F451007961667585%3Frf%3D145042235506367&width=132&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId" width="132" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/kerrywayultra?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @kerrywayultra</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
					<div class="carte_adresse">
						Paddywagon Tours Killarney<br>
						13 Main St<br>
						Killarney Co<br>
						IRLANDE
						<div style="display: flex;">
							<a href="tel:+353646630899">
								<div class="telephone">
									+353 64 663 0899
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FKerry-Way%2F451007961667585%3Frf%3D145042235506367&width=132&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId" width="132" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/kerrywayultra?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @kerrywayultra</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
					<div class="carte_adresse">
						The Fairview Hôtel<br>
						College St<br>
						Killarney Co<br>
						IRLANDE
						<div style="display: flex;">
							<a href="tel:+353646634164">
								<div class="telephone">
									+353 64 663 4164
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FKerry-Way%2F451007961667585%3Frf%3D145042235506367&width=132&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId" width="132" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/kerrywayultra?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @kerrywayultra</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
				</div>
			</div>

			<div>
				<?php
					include("les_treks/satisfaction/satisfaction.php");
				?>
			</div>

		</main>

		<footer>
			<?php include("footer/footer.php");?>
		</footer>

	<?php include('les_treks/mainpage_js.php'); ?>	
		
	</body>
</html>