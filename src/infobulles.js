/* ------MAP_IRLANDE------ */

$("#map_irlande").mousemove(function(){

	var infobulle = $("#infobulle_irlande");
	var x = event.pageX - (infobulle.width() / 2);
	var y = event.pageY - infobulle.height() - 20;
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$("#map_irlande").mouseout(function(){
	var infobulle = $('#infobulle_irlande');
	infobulle.css({
		display : 'none',
	});
});

/* ------MAP_ECOSSE------ */

$("#map_ecosse").mousemove(function(){

	var infobulle = $("#infobulle_ecosse");
	var x = event.pageX - (infobulle.width() / 2);
	var y = event.pageY - infobulle.height() - 20;
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$("#map_ecosse").mouseout(function(){
	var infobulle = $('#infobulle_ecosse');
	infobulle.css({
		display : 'none',
	});
});

/* ------MAP_SUISSE------ */

$("#map_suisse").mousemove(function(){

	var infobulle = $("#infobulle_suisse");
	var x = event.pageX - (infobulle.width() / 2);
	var y = event.pageY - infobulle.height() - 20;
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$("#map_suisse").mouseout(function(){
	var infobulle = $('#infobulle_suisse');
	infobulle.css({
		display : 'none',
	});
});

/* ------MAP_ITALIE------ */

$("#map_italie").mousemove(function(){

	var infobulle = $("#infobulle_italie");
	var x = event.pageX - (infobulle.width() / 2);
	var y = event.pageY - infobulle.height() - 20;
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$("#map_italie").mouseout(function(){
	var infobulle = $('#infobulle_italie');
	infobulle.css({
		display : 'none',
	});
});

/* ------MAP_CORSE------ */

$("#map_corse").mousemove(function(){

	var infobulle = $("#infobulle_corse");
	var x = event.pageX - (infobulle.width() / 2);
	var y = event.pageY - infobulle.height() - 20;
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$("#map_corse").mouseout(function(){
	var infobulle = $('#infobulle_corse');
	infobulle.css({
		display : 'none',
	});
});


/* ------GLASGOW------ */

$(".glasgow").mousemove(function(){

	var infobulle = $(".infobulle_glasgow");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".glasgow").mouseout(function(){
	var infobulle = $('.infobulle_glasgow');
	infobulle.css({
		display : 'none',
	});
});

/* ------FORT_WILLIAM------ */

$(".fort").mousemove(function(){

	var infobulle = $(".infobulle_fort");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".fort").mouseout(function(){
	var infobulle = $('.infobulle_fort');
	infobulle.css({
		display : 'none',
	});
});

/* ------GLASGOW------ */

$(".glasgow").mouseover(function(){

	var infobulle = $(".infobulle_glasgow");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".glasgow").mouseout(function(){
	var infobulle = $('.infobulle_glasgow');
	infobulle.css({
		display : 'none',
	});
});

/* ------MILNGAVIE------ */

$(".milngavie").mousemove(function(){

	var infobulle = $(".infobulle_milngavie");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".milngavie").mouseout(function(){
	var infobulle = $('.infobulle_milngavie');
	infobulle.css({
		display : 'none',
	});
});


/* ------CALENZANA------ */

$(".calenzana").mousemove(function(){

	var infobulle = $(".infobulle_calenzana");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".calenzana").mouseout(function(){
	var infobulle = $('.infobulle_calenzana');
	infobulle.css({
		display : 'none',
	});
});


/* ------CONCA------ */

$(".conca").mousemove(function(){

	var infobulle = $(".infobulle_conca");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".conca").mouseout(function(){
	var infobulle = $('.infobulle_conca');
	infobulle.css({
		display : 'none',
	});
});


/* ------PAYSAGE_GR20------ */

$(".paysage_gr20").mousemove(function(){

	var infobulle = $(".infobulle_paysage_gr20");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".paysage_gr20").mouseout(function(){
	var infobulle = $('.infobulle_paysage_gr20');
	infobulle.css({
		display : 'none',
	});
});


/* ------REFUGE_VIA------ */

$(".refuge_via").mousemove(function(){

	var infobulle = $(".infobulle_refuge_via");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".refuge_via").mouseout(function(){
	var infobulle = $('.infobulle_refuge_via');
	infobulle.css({
		display : 'none',
	});
});


/* ------TRENTIN------ */

$(".trentin").mousemove(function(){

	var infobulle = $(".infobulle_trentin");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".trentin").mouseout(function(){
	var infobulle = $('.infobulle_trentin');
	infobulle.css({
		display : 'none',
	});
});


/* ------VENETIE------ */

$(".venetie").mousemove(function(){

	var infobulle = $(".infobulle_venetie");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".venetie").mouseout(function(){
	var infobulle = $('.infobulle_venetie');
	infobulle.css({
		display : 'none',
	});
});


/* ------KILLARNEY------ */

$(".killarney").mousemove(function(){

	var infobulle = $(".infobulle_killarney");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".killarney").mouseout(function(){
	var infobulle = $('.infobulle_killarney');
	infobulle.css({
		display : 'none',
	});
});


/* ------RING_OF_KERRY------ */

$(".ring_of_kerry").mousemove(function(){

	var infobulle = $(".infobulle_ring_of_kerry");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".ring_of_kerry").mouseout(function(){
	var infobulle = $('.infobulle_ring_of_kerry');
	infobulle.css({
		display : 'none',
	});
});


/* ------BIVOUAC------ */

$(".bivouac").mousemove(function(){

	var infobulle = $(".infobulle_bivouac");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".bivouac").mouseout(function(){
	var infobulle = $('.infobulle_bivouac');
	infobulle.css({
		display : 'none',
	});
});


/* ------EIGER------ */

$(".eiger").mousemove(function(){

	var infobulle = $(".infobulle_eiger");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".eiger").mouseout(function(){
	var infobulle = $('.infobulle_eiger');
	infobulle.css({
		display : 'none',
	});
});


/* ------VALLEES------ */

$(".vallees").mousemove(function(){

	var infobulle = $(".infobulle_vallees");
	var x = event.pageX - infobulle.width() / 2;
	var y = event.pageY + 20;
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	if (x < 0) {
		x = 20
	};
	if (x + infobulle.width() > $(window).width()){
		x = $(window).width() - infobulle.width() - 50;
	};
	infobulle.css({
		top : y,
		left : x,
		display : 'flex',
	});
});

$(".vallees").mouseout(function(){
	var infobulle = $('.infobulle_vallees');
	infobulle.css({
		display : 'none',
	});
});