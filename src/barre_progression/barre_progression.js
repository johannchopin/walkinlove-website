var $ = jQuery.noConflict();
$(window).scroll(function() {
 
    //Calcule la hauteur de la page entière
    var h = $(document).height();
    //Détermine la position de la page
    var s = $(window).scrollTop();
    //Calcule la hauteur de page visible
    var w = $(window).height();
    
    //Formule mathématique pour calculer p le pourcentage de progression
    var t = (s / h) * w;
    var p = Math.ceil((s + t) / h * 110) + 1;
 
    $('#barre').width(p + '%');
    
});