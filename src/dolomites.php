<?php

	$trek_name = "Alta Via des Dolomites";

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<link rel="stylesheet" type="text/css" href="les_treks/dolomites.css">

		<?php include("les_treks/mainpage_head.php"); ?>

		<title>WalkInLove - Nos Treks</title>
	</head>
	<body style="margin: 0; padding: 0;">
		

		<header>
			<?php include("header/header_mini.php");?>			
		</header>

		<main>
			<?php include('go_up/go_up.php');?>

			<div class="map">
				<img alt="localisation dolomites" src="img/les_treks_img/dolomites.png">
			</div>

			<div class="dolomites_title">
				<h3>#3<br>- ALTA VIA DES DOLOMITES (ITALIE) -</h3>
			</div>

			<div>
				<div class="treks_intro">
					&nbsp;&nbsp;Entre le Lac de Braies dans le <span class="trentin">Trentin</span> et Belluno en <span class="venetie">Vénétie</span>, l’Alta Via n°1 chemine plus de <span class="important">130 km</span> à travers les Dolomites italiennes. Sous les sommets acérés et les voies aériennes, les randonneurs sont accueillis comme des princes dans les auberges et les <span class="refuge_via">refuges de montagne</span>. Une Haute Route, littéralement. Reculée, préservée et panoramique !
				</div>
			</div>

			<!-- INFOBULLE_TEXTE -->

			<div class="infobulle_container">
				<div class="infobulle_refuge_via">
					<img alt="photo d'un refuge" src="img/les_treks_img/refuge_via.png">
					<div>
						<div class="infobulle_text">
							&nbsp;Vue grandiose au dessus des sommets des Dolomites depuis le refuge Lagazuoi où les randonneurs peuvent loger.
						</div>
					</div>
				</div>
				<div class="infobulle_trentin">
					<img alt="vue sur Trentin" src="img/les_treks_img/trentin.png">
					<div>
						<div class="infobulle_text">
							&nbsp;Sortez des sentiers battus et trouvez en Trentin-Haut-Adige un véritable bijou de la nature. Visitez un environnement encore pur et sauvage, qui compte pas moins de 600 lacs et de spectaculaires paysages montagneux.
						</div>
					</div>
				</div>
				<div class="infobulle_venetie">
					<img alt="vue sur Venise" src="img/les_treks_img/venetie.png">
					<div>
						<div class="infobulle_text">
							&nbsp;La Vénétie est une région du nord-est de l'Italie s'étendant des Dolomites à la mer Adriatique. Venise, sa capitale, est célèbre pour ses canaux et son architecture gothique.
						</div>
					</div>
				</div>
			</div>

			<!-- FIN_INFOBULLE_TEXTE -->
			
			
			<div class="carrousel_container">
				<div style="margin-bottom: 20px;">
					<div style="width: 50px;border-bottom: solid white 2px; margin: auto;"></div>
				</div>
				<div class="swiper-container_2">
				    <div class="swiper-wrapper">
				    	<div class="swiper-slide" id="swiper-slide_2" style="background-image:url(img/way_carrousel_1.png)"></div>
				    	<div class="swiper-slide" id="swiper-slide_2" style="background-image:url(img/way_carrousel_2.png)"></div>
				    	<div class="swiper-slide" id="swiper-slide_2" style="background-image:url(img/way_carrousel_3.png)"></div>
				    </div>
				    <!-- Add Pagination -->
				    <div class="swiper-pagination swiper-pagination-white"></div>
				    <div class="swiper-button-next"></div>
				    <div class="swiper-button-prev"></div>
				</div>
				<div style="margin-top: 20px;">
					<div style="width: 50px;border-bottom: solid white 2px; margin: auto;"></div>
				</div>
			</div>


			<div class="titre_section">
				<div style="display: flex; width: auto;">
					<img alt="icon 'lieu'" src="img/lieu_icon.png" style="width: 50px; height: 60px;">
					<div style="display: flex; align-items: center;">
						<div>
							Lieu de départ
						</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
				<div class="destination">
					Belluno
				</div>
			</div>

			<div style="display: flex;">
				<div class="bordure">
					<img alt="icon d'avion" src="img/avion_icon.png">
					<div class="delimitation_avion"></div>
				</div>
				<div class="trajet_contain">
					<iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d2790212.528680833!2d5.082676514683654!3d46.925981164663895!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x47e66e1f06e2b70f%3A0x40b82c3688c9460!2sParis!3m2!1d48.856614!2d2.3522219!4m5!1s0x477907a4819f1c33%3A0xed2742f66d62cf2c!2sBelluno%2C+32100+Belluno%2C+Italie!3m2!1d46.1424635!2d12.2167088!5e0!3m2!1sfr!2sfr!4v1515090475204" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					<div>
						<a href="https://www.airfrance.fr" target="blanc"><img alt="airfrance logo" src="img/airfrance_logo.png"></a>
						<a href="http://www.volotea.com/fr" target="blanc"><img alt="volotea logo" src="img/volotea_logo.png"></a>
						<a href="http://www.liligo.fr" target="blanc"><img src="img/liligo_logo.png"></a>
						<a href="https://www.easyvoyage.com" target="blanc"><img alt="easyvoyage logo" src="img/easyvoyage_logo.png"></a>
					</div>
				</div>
			</div>

			<div class="titre_section">
				<div style="display: flex; margin: auto;">
					<img alt="icon séjour" src="img/sejour_icon.png" style="width: 50px; height: 50px;">
					<div>
						<div>Déroulement du Trek</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
			</div>

			<div style="width: 100%; display: flex;">	
				<div class="bordure">
					<img alt="icon sac à dos" src="img/voyage_icon.png">
					<div class="delimitation_sejour"></div>
				</div>
				<div style="margin: auto; display: flex;" class="table">
					<table cellspacing="30" cellpadding="5">
						<thead>
							<tr>
								<th>Etapes</th>
								<th>Description</th>
								<th>Longueur</th>
								<th>Durée</th>
								<th>Niveau</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>#1</td>
								<td>Belluno - La Stanga</td>
								<td>15 km</td>
								<td>5 - 6 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#2</td>
								<td>La Stanga - Passo Duran</td>
								<td>13 km</td>
								<td>5 heures</td>
								<td>**</td>
							</tr>
							<tr>
								<td>#3</td>
								<td>Passo Duran - Staulanza</td>
								<td>22 km</td>
								<td>9 - 10 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#4</td>
								<td>Staulanza - Cortina</td>
								<td>10 km</td>
								<td>5 - 6 heures</td>
								<td>*</td>
							</tr>
							<tr>
								<td>#5</td>
								<td>Cortina - Passo</td>
								<td>9 km</td>
								<td>4 - 5 heures</td>
								<td>**</td>
							</tr>
							<tr>
								<td>#6</td>
								<td>Passo - Lago di Braies</td>
								<td>17 km</td>
								<td>7 - 9 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#7</td>
								<td>Dobbiaco - Lago di Braies</td>
								<td>9 km</td>
								<td>5 - 6 heures</td>
								<td>**</td>
							</tr>
						</tbody>
					</table>
					<div style="display: flex;">
						<img alt="marche alta via des dolomites map" src="img/les_treks_img/via_road.png">
					</div>
					</div>
			</div>

			<div class="titre_section">
				<div style="display: flex; margin: auto;">
					<img alt="icon carte" src="img/information2_icon.png" style="width: 50px; height: 50px;">
					<div>
						<div>Pour des informations supplémentaires</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
			</div>

			<div style="width: 100%; display: flex;">
				<div class="bordure">
					<img alt="icon information" src="img/information_icon.png">
					<div class="delimitation_information"></div>
				</div>
				<div class="information_contain">
					<div class="carte_adresse">
						Association touristique Wolkenstein<br>
						213 Strada Mesules<br>
						39048 Selva di Val Gardena BZ<br>
						ITALIE
						<div style="display: flex;">
							<a href="tel:+390471839695">
								<div class="telephone">
									+39 0471 839695
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Faltavia2dolomiti%2F&width=152&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId" width="152" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/altaviadue?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @altaviadue</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
					<div class="carte_adresse">
						Office de tourisme Alta Badia<br>
						75 Strada Colz<br>
						39030 La Villa BZ<br>
						ITALIE
						<div style="display: flex;">
							<a href="tel:+390471847037">
								<div class="telephone">
									+39 0471 847037
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Faltavia2dolomiti%2F&width=152&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId" width="152" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/altaviadue?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @altaviadue</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
					<div class="carte_adresse">
						Hotel Leitlhof - Dolomiten<br>
						29 Via Pusteria<br>
						39038 San Candido BZ<br>
						ITALIE
						<div style="display: flex;">
							<a href="tel:+390474913440">
								<div class="telephone">
									+39 0474 913440
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Faltavia2dolomiti%2F&width=152&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId" width="152" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/altaviadue?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @altaviadue</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
				</div>
			</div>

			<div>
				<?php
					include("les_treks/satisfaction/satisfaction.php");
				?>
			</div>

		</main>

		<footer>
			<?php include("footer/footer.php");?>
		</footer>


	<?php include('les_treks/mainpage_js.php'); ?>	
		
	</body>
</html>