<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="icon" type="image/png" href="img/favicon.png">
		<link rel="stylesheet" type="text/css" href="index.css">
		<link rel="stylesheet" type="text/css" href="font/font.css">
		<link rel="stylesheet" type="text/css" href="main.css">
		<link rel="stylesheet" type="text/css" href="classement.css">
		<link rel="stylesheet" type="text/css" href="footer/footer.css">
		<link rel="stylesheet" type="text/css" href="menu/menu.css">
		<link rel="stylesheet" type="text/css" href="go_up/go_up.css">
		<link rel="stylesheet" type="text/css" href="barre_progression/barre_progression.css">

		<title>WalkInLove</title>

		<style>
			footer{
				position: absolute;
				top: 4500px;
				margin: 0;
			}
			@media screen and (max-width: 1024px){
				footer{
					top: 9150px;
				}
			}
			@media screen and (max-width: 800px){
				footer{
					top: 9300px;
				}
			}
		</style>
	</head>

	<body>
		
		<?php include("barre_progression/barre_progression.php");?>	

		<?php include("main_map/map.php");?>


		<header>
			<div class="fondvideoparent">
				<div class="fondlogoparent">				
					<div class="logoparent">
						<img alt="logo de montagne" src="img/logo.png" id="test2" class="logo">
					</div>
				</div>
				<img alt="image de fond (paysage)" src="img/background_title.png">
			</div>
		</header>


			<?php include('go_up/go_up.php');?>
		<main>

			<div class="title">
				<h1>Les 5 plus beaux <br>
				treks d'Europe
				</h1>
			</div>

			<div class="presentationparent">
				<p class="presentation"><img src="img/flottant_presentation.png" class="imgfloatmontain" alt="Image Flottante Montagne"> 
				<p class="presentation"><h2>Les randonnées et le camping ne vous font pas peur ?</h2>
				<p class="presentation">Vous êtes alors fait pour le trek, ces randonnées de plusieurs jours permettant de partir à la découverte de lieux insoupçonnés aux quatre coins de l'Europe.
				<br><br> 
				En 
				<a href="bernois.php">Suisse</a>, en 
				<a href="gr20.php">Écosse</a> ou encore en 
				<a href="dolomites.php">Italie</a>, nous avons adapté nos choix à ceux qui possèdent une bonne condition physique mais également aux novices qui cherchent à se dépasser.<br><br>
				<a href="index.php" class="a_walkinlove">WalkInLove</a> 
				vous proposent sa sélection des 5 treks à faire absolument une fois dans sa vie en Europe.</p>
			</div>

			<div class="carte_parent">
				<div class="carte_enfant">
					<img alt="map de l'europe" src="img/carteeurope2.png" class="carte" usemap="#europemap">

					<map class="big_screen_map" name="europemap">
						<area shape="poly" coords="60,340,100,315,110,370,50,400" 
						alt="Kerry Way"  
						href="kerry.php"
						id="map_irlande">

						<area shape="rect" coords="110,250,190,420" 
						alt="West Higland Way" 
						href="way.php"
						id="map_ecosse">

						<area shape="rect" coords="280,560,300,600" 
						alt="Le GR20"
						href="gr20.php"
						id="map_corse">
						
						<area shape="poly" coords="260,470,300,470,310,500,260,520,230,500" 
						alt="Oberland Bernois" 
						href="bernois.php"
						id="map_suisse">
						
						<area shape="poly" coords="330,490,250,530,400,650,440,600" 
						alt="Alta Via 1 des Dolomites" 
						href="dolomites.php"
						id="map_italie">
					</map>
				</div>
			</div>


			<nav>
				<?php include("menu/menu.php");?>
			</nav>
			

			<div class="classementparent">
				<div class="classementcontainer">
					<a href="gr20.php">
						<div class="classement1">
							<div class="gr20imgcontainer">
								<img alt="vue du gr20" src="img/gr20.png" class="gr20img">
							</div>
							<div style="display: flex;">
								<div class="classementtitle">
									<h3>#1<br>- Le GR20 en Corse (FRANCE) -</h3>
									<p>Petit passage par la Corse pour ce trek d’exception. Ouvert en 1972, le GR20 relie Calenzana (au Nord) à Conca (au Sud), et se fait en 14 jours environ. Réputé comme exigeant, ce mythique sentier de randonnée regorge de paysages époustouflants et de trésors, dont de nombreux lacs, cascades et piscines naturelles.</p>
								</div>
							</div>
						</div>
					</a>

					<a href="way.php">
						<div class="classement2">
							<div class="wayimgcontainer">
								<img alt="vue du W.H.Way" src="img/way.png"  id="imgstyle1" class="wayimg">
							</div>
							<div style="display: flex;">
								<div class="classementtitle">
									<h3>#2<br>- West Higland Way (Écosse) -</h3>
									<p>Partir pour la randonnée la plus célèbre d’Écosse, c’est découvrir l'immensité et la diversité de paysages à couper le souffle ! Ce fameux parcours s’enfonce au cœur des Highlands, contourne le splendide Loch Lomond, pour atteindre la montagne du Ben Nevis, point culminant de Grande Bretagne à 1 344 m.</p>
								</div>
							</div>
						</div>
					</a>

					<a href="dolomites.php">
						<div class="classement3">
							<div class="viaimgcontainer">
								<img alt="vue des dolomites" src="img/via.png" class="viaimg">
							</div>
							<div style="display: flex;">
								<div class="classementtitle">
									<h3>#3<br>- Alta Via des Dolomites (Italie) -</h3>
									<p>Partir pour la randonnée la plus célèbre d’Écosse, c’est découvrir l'immensité et la diversité de paysages à couper le souffle ! Ce fameux parcours s’enfonce au cœur des Highlands, contourne le splendide Loch Lomond, pour atteindre la montagne du Ben Nevis, point culminant de Grande Bretagne à 1 344 m.</p>
								</div>
							</div>
						</div>
					</a>

					<a href="kerry.php">
						<div class="classement4">
							<div class="kerryimgcontainer">
								<img alt="vue du kerry way" src="img/kerry.png" class="kerryimg">
							</div>
							<div style="display: flex;">
								<div class="classementtitle">
									<h3>#4<br>- kerry Way (Irlande) -</h3>
									<p>Ce magnifique circuit de randonnée démarre et s’achève à Killarney, et permet de découvrir les beautés du Ring of Kerry, ainsi que la culture gaélique, toujours bien ancrée aujourd’hui. Les côtes déchiquetées irlandaises sont époustouflantes par leur beauté sauvage et leurs couleurs alternant camaïeux de verts et de bleus. Côté bivouac, vous aurez l’embarras du choix !</p>
								</div>
							</div>
						</div>
					</a>

					<a href="bernois.php">
						<div class="classement5">
							<div class="bernoisimgcontainer">
								<img alt="vue sur les montagne d'Oberland Bernois" src="img/bernois.png" class="bernoisimg">
							</div>
							<div style="display: flex;">
								<div class="classementtitle">
									<h3>#5<br>- Oberland Bernois (Suisse) -</h3>
									<p>La région de l’Oberland bernois est l’une des plus diversifiées de Suisse et attire depuis longtemps touristes et alpinistes. Ses montagnes mythiques, Eiger, Mönch et Jungfrau, forment une trilogie de sommets à plus de 4 000 m. Les sentiers offrent une infinie variété de paysages de montagne : balcons, vallées verdoyantes, villages traditionnels et fleuris, lacs d'altitude... Un voyage inoubliable !</p>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>

		</main>

		<footer>

			<?php include("footer/footer.php");?>
		
		</footer>

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script type="text/javascript" src="slide.js"></script>
	<script type="text/javascript" src="go_up/go_up.js"></script>
	<script type="text/javascript" src="infobulles.js"></script>
	<script type="text/javascript" src="footer/footer_sessionStorage.js"></script>
	<script type="text/javascript" src="barre_progression/barre_progression.js"></script>
	<script type="text/javascript" src="footer/footer.js"></script>

	</body>
</html>