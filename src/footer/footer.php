<?php

$error_nom = 'error_none';
$error_mail = 'error_none';
$error_text = 'error_none';

$error_list = '';

if(isset($_POST['contact_envoyer'])){

	if (strlen($_POST['contact_name']) < 2){
		$error_nom = 'error';
		$error_nom_text = "<li>entré votre nom à 2 lettres minimum</li>";
		$error_list = $error_list.$error_nom_text;
	}
	else{
		$error_nom = 'error_none';
	};

	if (strlen($_POST['contact_mail']) == 0 OR strpos($_POST['contact_mail'], ' ') !== false){
		$error_mail = 'error';
		$error_mail_text = "<li>entré une adresse valide pour que l'on puisse vous recontacter</li>";
		$error_list = $error_list.$error_mail_text;
	}
	else{
		$error_mail = 'error_none';
	};

	if (strlen($_POST['contact_text']) == 0) {
		$error_text = 'error';
		$error_text_text = "<li>saisi la raison de votre contact</li>";
		$error_list = $error_list.$error_text_text;
	}
	else{
		$error_text = 'error_none';
	};

  	if($error_list == '') { 
	  	file_put_contents('footer/footer_donnees.txt',
	        ucwords(strtolower(trim($_POST['contact_name']))).' '.
			ucwords(strtolower(trim($_POST['contact_mail']))).' '.
			ucwords(strtolower(trim($_POST['contact_text']))), FILE_APPEND | LOCK_EX);
    }

};


?>



<div>
	<p>Suivez-nous :</p>
	<div style="display: flex; margin: 0px; padding: 0px;">
		<a href="https://fr-fr.facebook.com/paysagedemontagne/" target="_blank"><img alt="facebook icon" class="social" id="facebook" src="img/facebook.png"></a>
		<a href="https://www.instagram.com/Alexstrohl/" target="_blank"><img alt="instagram icon" class="social" id="instagram" src="img/instagram.png"></a>
		<a href="https://twitter.com/search?q=%23montagne" target="_blank"><img alt="twitter icon" class="social" id="twitter" src="img/twitter.png"></a>
		<a href="https://www.pinterest.fr/francemontagnes/nature-en-montagne/" target="_blank"><img alt="pinterest icon" class="social" id="pinterest" src="img/pinterest.png"></a>
	</div>
</div>
<div class="footer_lien">
	<p>Liens interessants :</p>
	<ul>
		<li><a href="http://www.routard.com/" target="_blank">Le Guide du Routard</a></li>
		<li><a href="http://www.allibert-trekking.com/" target="_blank">Allibert trekking</a></li>
		<li><a href="https://voyages.michelin.fr/" target="_blank">Michelin Voyage</a></li>
	</ul>
</div>
<div>
	<p>Contactez-nous :</p>
	
	<form action="#" method="post">
		<input type="text" placeholder="Nom" name="contact_name" class="<?php echo $error_nom; ?>"
		value="<?php if($error_list !== ''){  echo $_POST['contact_name']; }?>"><br>
		<input type="email" placeholder="votremail@mail.com" name="contact_mail" class="<?php echo $error_mail; ?>"
		value="<?php if($error_list !== ''){  echo $_POST['contact_mail']; }?>"><br>
		<textarea placeholder="Votre texte..." name="contact_text" class="<?php echo $error_text; ?>"><?php if($error_list !== ''){  echo $_POST['contact_text']; }?></textarea><br>
		<input class="error_none" type="submit" value="- Envoyer -" name="contact_envoyer"  onclick="formulaire()">
	</form>
</div>

<div id="formulaire_alert_container" class="<?php if(isset($_POST['contact_envoyer'])){
		echo('formulaire_alert');
	}
	else{
		echo('delete');
	} ?>">
	<div class="<?php if($error_list !== ''){
			echo('alert_probleme'); 
		}
		else{
			echo('delete');			
		}?>">
		<div class="titre_alert">
			Echec de l'envoi
		</div>
		<div>
			! Oups il semblerait qu'une erreur soit survenue, vous n'avez malheureusement pas :
		</div>
		<ul>
			<?php 
				echo $error_list;
			?>			
		</ul>
		<div class="button_alert_container">
			<div class="alert_button1" onclick="formulaire()">Completer le formulaire</div>
			<a href=""><div class="alert_button2">Revenir sur la page</div></a>
		</div>
	</div>
	<div class="<?php if($error_list == ''){
			echo('alert_valide'); 
		}
		else{
			echo('delete');			
		}?>">
		<div class="titre_alert">
			Envoi réussi
		</div>
		<div class="validation_message">
			Votre message a bien été envoyé.<br>
			Merci pour votre temps et bonne rando.
		</div>
		<div class="button_alert_container">
			<a href=""><div class="alert_button2">Revenir sur la page</div></a>
		</div>
	</div>
</div>