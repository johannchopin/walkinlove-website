<?php

	$trek_name = "Oberland Bernois";

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<link rel="stylesheet" type="text/css" href="les_treks/bernois.css">

		<?php include("les_treks/mainpage_head.php"); ?>

		<title>WalkInLove - Nos Treks</title>
	</head>
	<body style="margin: 0; padding: 0;">
		

		<header>
			<?php include("header/header_mini.php");?>			
		</header>

		<main>
			<?php include('go_up/go_up.php');?>

			<div class="map">
				<img alt="localisation oberland bernois" src="img/les_treks_img/bernois.png">
			</div>

			<div class="bernois_title">
				<h3>#5<br>- OBERLAND BERNOIS (SUISSE) -</h3>
			</div>

			<div>
				<div class="treks_intro">
					&nbsp;&nbsp;La région de l’Oberland bernois est l’une des plus diversifiées de Suisse et attire depuis longtemps touristes et alpinistes. Ses montagnes mythiques, <span class="eiger">Eiger</span>, Mönch et Jungfrau, forment une trilogie de sommets splendide. Le sentier de près de <span class="important">172 km</span> offrent une infinie variété de paysages de montagne : balcons, <span class="vallees">vallées verdoyantes</span>, villages traditionnels et fleuris, lacs d'altitude...
				</div>
			</div>

			<!-- INFOBULLE_TEXTE -->

			<div class="infobulle_container">	
				<div class="infobulle_eiger">
					<img alt="vue sur l'Eiger" src="img/les_treks_img/eiger.png">
					<div>
						<div class="infobulle_text">
							&nbsp;La face nord de l'Eiger, dans l'Oberland bernois, est l'une des trois grandes faces nord des Alpes. L'Eiger est traversé par le tunnel d'un chemin de fer à crémaillère de la compagnie de la Jungfraubahn. Il existe une gare dans la paroi de la face nord, appelée Eigerwand, qui permet d'avoir une vue au sein de la face nord.
						</div>
					</div>
				</div>
				<div class="infobulle_vallees">
					<img alt="vue sur une vallée de l'Oberland Bernois" src="img/les_treks_img/vallees.png">
					<div>
						<div class="infobulle_text">
							&nbsp;Le contraste entre les vallées verdoyantes et les montagnes de l'Oberland Bernois en font toute sa richesse panoramique.
						</div>
					</div>
				</div>
			</div>

			<!-- FIN_INFOBULLE_TEXTE -->
			
			
			<div class="carrousel_container">
				<div style="margin-bottom: 20px;">
					<div style="width: 50px;border-bottom: solid white 2px; margin: auto;"></div>
				</div>
				<div class="swiper-container_2">
				    <div class="swiper-wrapper">
				    	<div class="swiper-slide" id="swiper-slide_2" style="background-image:url(img/way_carrousel_1.png)"></div>
				    	<div class="swiper-slide" id="swiper-slide_2" style="background-image:url(img/way_carrousel_2.png)"></div>
				    	<div class="swiper-slide" id="swiper-slide_2" style="background-image:url(img/way_carrousel_3.png)"></div>
				    </div>
				    <!-- Add Pagination -->
				    <div class="swiper-pagination swiper-pagination-white"></div>
				    <div class="swiper-button-next"></div>
				    <div class="swiper-button-prev"></div>
				</div>
				<div style="margin-top: 20px;">
					<div style="width: 50px;border-bottom: solid white 2px; margin: auto;"></div>
				</div>
			</div>


			<div class="titre_section">
				<div style="display: flex; width: auto;">
					<img alt="icon lieu" src="img/lieu_icon.png" style="width: 50px; height: 60px;">
					<div style="display: flex; align-items: center;">
						<div>
							Lieu de départ
						</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
				<div class="destination">
					Lugano
				</div>
			</div>

			<div style="display: flex;">
				<div class="bordure">
					<img alt="icon d'avion" src="img/avion_icon.png">
					<div class="delimitation_avion"></div>
				</div>
				<div class="trajet_contain">
					<iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d4592524.461863536!2d5.930754361940003!3d47.04860791718988!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x47e66e1f06e2b70f%3A0x40b82c3688c9460!2sParis!3m2!1d48.856614!2d2.3522219!4m5!1s0x47842df76a4211f1%3A0xef8c04212ea1f8e0!2sLugano%2C+Suisse!3m2!1d46.0036778!2d8.951051999999999!5e0!3m2!1sfr!2sfr!4v1515149062731" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					<div>
						<a href="https://www.airfrance.fr" target="blanc"><img alt="airfrance logo" src="img/airfrance_logo.png"></a>
						<a href="http://www.volotea.com/fr" target="blanc"><img alt="volotea logo" src="img/volotea_logo.png"></a>
						<a href="http://www.liligo.fr" target="blanc"><img alt="liligo logo" src="img/liligo_logo.png"></a>
						<a href="https://www.easyvoyage.com" target="blanc"><img alt="easyvoyage logo" src="img/easyvoyage_logo.png"></a>
					</div>
				</div>
			</div>

			<div class="titre_section">
				<div style="display: flex; margin: auto;">
					<img alt="icon séjour" src="img/sejour_icon.png" style="width: 50px; height: 50px;">
					<div>
						<div>Déroulement du Trek</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
			</div>

			<div style="width: 100%; display: flex;">	
				<div class="bordure">
					<img alt="icon sac à dos" src="img/voyage_icon.png">
					<div class="delimitation_sejour"></div>
				</div>
				<div style="margin: auto; display: flex;" class="table">
					<table cellspacing="30" cellpadding="5">
						<thead>
							<tr>
								<th>Etapes</th>
								<th>Description</th>
								<th>Longueur</th>
								<th>Durée</th>
								<th>Niveau</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>#1</td>
								<td>Oberwald - lac d’Oberaar</td>
								<td>8 km</td>
								<td>3 heures</td>
								<td>**</td>
							</tr>
							<tr>
								<td>#2</td>
								<td>Lac d’Oberaar - refuge Oberaarjoch</td>
								<td>12 km</td>
								<td>5 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#3</td>
								<td>Refuge Oberaarjoch - refuge de Finsteraarhorn</td>
								<td>12 km</td>
								<td>6 - 7 heures</td>
								<td>****</td>
							</tr>
							<tr>
								<td>#4</td>
								<td>Refuge du Finsteraarhorn - refuge Konkordia</td>
								<td>10 km</td>
								<td>5 heures</td>
								<td>**</td>
							</tr>
							<tr>
								<td>#5</td>
								<td> Refuge de Konkordia -refuge Hollandia</td>
								<td>9 km</td>
								<td>5 heures</td>
								<td>**</td>
							</tr>
							<tr>
								<td>#6</td>
								<td>Refuge Hollandia - Goppenstein</td>
								<td>17 km</td>
								<td>7 - 8 heures</td>
								<td>****</td>
							</tr>
						</tbody>
					</table>
					<div style="display: flex;">
						<img alt="marche bernois map" src="img/les_treks_img/bernois_road.png">
					</div>
					</div>
			</div>

			<div class="titre_section">
				<div style="display: flex; margin: auto;">
					<img alt="icon carte" src="img/information2_icon.png" style="width: 50px; height: 50px;">
					<div>
						<div>Pour des informations supplémentaires</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
			</div>

			<div style="width: 100%; display: flex;">
				<div class="bordure">
					<img alt="icon information" src="img/information_icon.png">
					<div class="delimitation_information"></div>
				</div>
				<div class="information_contain">
					<div class="carte_adresse">
						Lugano Turismo<br>
						15 Via Aeroporto<br>
						6982 Agno<br>
						SUISSE
						<div style="display: flex;">
							<a href="tel:+41916051226">
								<div class="telephone">
									+41 91 605 12 26
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FOberland-bernois%2F712012732166735&width=139&layout=button_count&action=like&size=small&show_faces=true&share=true&height=46&appId" width="139" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/suisse?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @suisse</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
					<div class="carte_adresse">
						Office Du Tourisme - Bellagio<br>
						48 Piazza Giuseppe Mazzini<br>
						22021 Bellagio<br>
						ITALIE
						<div style="display: flex;">
							<a href="tel:+390341830367">
								<div class="telephone">
									+39 0341 830367
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FOberland-bernois%2F712012732166735&width=139&layout=button_count&action=like&size=small&show_faces=true&share=true&height=46&appId" width="139" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/suisse?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @suisse</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
					<div class="carte_adresse">
						Ente Turistico del Mendrisiotto e Basso Ceresio<br>
						2 Via Luigi Lavizzari<br>
						6850 Mendrisio<br>
						SUISSE
						<div style="display: flex;">
							<a href="tel:+41916413050">
								<div class="telephone">
									+41 91 641 30 50
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FOberland-bernois%2F712012732166735&width=139&layout=button_count&action=like&size=small&show_faces=true&share=true&height=46&appId" width="139" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/suisse?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @suisse</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
				</div>
			</div>

			<div>
				<?php
					include("les_treks/satisfaction/satisfaction.php");
				?>
			</div>

		</main>

		<footer>
			<?php include("footer/footer.php");?>
		</footer>


	<?php include('les_treks/mainpage_js.php'); ?>	
		
	</body>
</html>