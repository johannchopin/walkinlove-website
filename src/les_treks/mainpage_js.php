
      <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
      <script type="text/javascript" src="slide/slide.js"></script>
      <script type="text/javascript" src="les_treks/satisfaction/satisfaction.js"></script>
      <script type="text/javascript" src="infobulles.js"></script>
      <script type="text/javascript" src="Swipe/swipe.js"></script>
      <script type="text/javascript" src="header/menu_icon.js"></script>
      <script type="text/javascript" src="footer/footer.js"></script>
      <script type="text/javascript" src="go_up/go_up.js"></script>
      <script>

      var swiper = new Swiper('.swiper-container_2', {
        effect: 'flip',
        autoplay: true,
        loop: true,
        pagination: {
          el: '.swiper-pagination',
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });

      var swiper = new Swiper('.swiper-container_3', {
        effect: 'cube',
        speed: 1000,
        allowTouchMove: false,
        cubeEffect: {
          shadow: false,
        },
        navigation: {
          nextEl: '#next_button',
          prevEl: '#prev_button',
        },
      });
    </script>