<?php

/* 

	IMPORTANT : mon code satisfaction.js m'assure que l'utilisateur à répondu
	à toute les questions. Je n'ai donc pas besoin de toutes les tester mais juste
	de connaitre les réponses.


*/

$suivant_active = false;
$nbr_rando = 0;
$gr20_checked = false;
$way_checked = false;
$via_checked = false;
$kerry_checked = false;
$bernois_checked = false;
$aucun = false;
$note_du_site = 0;
$nothing_to_say_checked = false;
$satisfaction_text = false;
$satisfaction_no_text = false;
$validation_message = false;


if(isset($_POST['satisfaction_submit'])){

	$suivant_active = true;

	if (isset($_POST['gr20'])) {
		$nbr_rando += 1;
		$gr20_checked = true;
	};

	if (isset($_POST['way'])) {
		$nbr_rando += 1;
		$way_checked = true;
	};

	if (isset($_POST['via'])) {
		$nbr_rando += 1;
		$via_checked = true;
	};

	if (isset($_POST['kerry'])) {
		$nbr_rando += 1;
		$kerry_checked = true;
	};

	if (isset($_POST['bernois'])) {
		$nbr_rando += 1;
		$bernois_checked = true;
	};

	if (isset($_POST['aucun'])) {
		$aucun = true;
	};

	if ($_POST['radio_visage'] == "nul") {
		$note_du_site = 1;
	}

	elseif ($_POST['radio_visage'] == 'moyen') {
		$note_du_site = 2;
	}
	if ($_POST['radio_visage'] == 'bien'){
		$note_du_site = 3;
	};

	if (isset($_POST['nothing_to_say'])) {
		$satisfaction_no_text = true;
		$satisfaction_text_final = 'Pas de commentaire laissé';
	}
	elseif (strlen($_POST['say_text']) >= 1) {
		$satisfaction_text = true;
		$satisfaction_text_final = $_POST['say_text'];
	}
	else{

		$satisfaction_text_final = 'Pas de commentaire laissé';
	};

	file_put_contents('les_treks/satisfaction/satisfaction_donnee.txt',
	        '---Nombre de treks effectué : '.ucwords(strtolower(trim($nbr_rando))).' '.
			' // Note donnée au site : '.ucwords(strtolower(trim($note_du_site))).' '.
			' // Commentaire : '.strtolower(trim($satisfaction_text_final)).' '.
			' // Raison de la venue sur le site : '.ucwords(strtolower(trim($_POST['visite']))).'---', FILE_APPEND | LOCK_EX);

	$validation_message = true;

};


?>

<div class="satisfaction_container">
	<div class="satisfaction_titre">
		<div>
			<span style="font-size: 0.6em;">Merci de vous être intéressés à notre présentation sur</span><br>
			<div class="nom_trek">
				<?php 

					echo "- " . $trek_name . " -";

				?>
			</div>
			<div style="font-size: 0.6em; margin-bottom: 15px;">Participez maintenant à notre</div>
			<span style="font-size: 2em;">ENQUÊTE</span><br>
			DE 
			<span style="border-bottom: solid white 3px; padding-bottom: 0px;">SATISFACTION</span><br>
		</div>
		
	</div>

	<form method="post" action="#">
		<div class="carrousel_container">
			<div class="swiper-container_3">
			    <div class="swiper-wrapper">
			        <div class="swiper-slide" id="swiper-slide_3">
			    		
			    		<div class="question">
			    			Avez-vous déjà fait un de ces treks ?<br>
			    			Si oui lesquels :
			    		</div>
		    			<div class="suggestion1">
		    				<div>
		    					<label>
				    				<input type="checkbox" name="gr20" id="gr20_checkbox">
				    				<span
				    					<?php if ($gr20_checked AND $aucun <> true) {
				    						echo "style='border: dashed 4px green; box-shadow: none;'";
				    					}; ?> 
				    					class="checkbox_gr20" id="checkbox_gr20" onclick="checkbox_gr20_anim(); suivant_animation(this)">Le G20</span>
				    			</label><br>
				    			<label>
				    				<input type="checkbox" name="way" id="way_checkbox">
				    				<span 
				    					<?php if ($way_checked AND $aucun <> true) {
				    						echo "style='border: dashed 4px green; box-shadow: none;'";
				    					};  ?> 
				    					class="checkbox_way" id="checkbox_way" onclick="checkbox_way_anim(); suivant_animation(this)">West Higland Way</span>
				    			</label><br>
				    			<label>
				    				<input type="checkbox" name="via" id="via_checkbox">
				    				<span 
				    					<?php if ($via_checked AND $aucun <> true) {
				    						echo "style='border: dashed 4px green; box-shadow: none;'";
				    					};  ?> 
				    					class="checkbox_via" id="checkbox_via" onclick="checkbox_via_anim(); suivant_animation(this)">Alta Via des Dolomites</span>
				    			</label><br>
				    			<label id="test">
				    				<input type="checkbox" name="kerry" id="kerry_checkbox">
				    				<span 
				    					<?php if ($kerry_checked AND $aucun <> true) {
				    						echo "style='border: dashed 4px green; box-shadow: none;'";
				    					};  ?> 
				    					class="checkbox_kerry" id="checkbox_kerry" onclick="checkbox_kerry_anim(); suivant_animation(this)">Kerry Way</span>
				    			</label><br>
				    			<label>
				    				<input type="checkbox" name="bernois" id="bernois_checkbox">
				    				<span 
				    					<?php if ($bernois_checked AND $aucun <> true) {
				    						echo "style='border: dashed 4px green; box-shadow: none;'";
				    					};  ?> 
				    					class="checkbox_bernois" id="checkbox_bernois" onclick="checkbox_bernois_anim(); suivant_animation(this)">Oberland Bernois</span>
				    			</label><br>
				    			<label>
				    				<input type="checkbox" name="aucun" id="aucun_checkbox">
				    				<span
				    					<?php if ($aucun) {
				    						echo "style='border: dashed 4px green; box-shadow: none;'";
				    					};  ?>
				    					class="checkbox_aucun" id="checkbox_aucun" onclick="checkbox_aucun_anim(); suivant_animation(this)">Aucun</span>
				    			</label>
			    			</div>
		    			</div>
			    
				    </div>


			    	<div class="swiper-slide" id="swiper-slide_3">

			    		<div class="question">
			    			Notre site vous a été utile ?<br>
			    			Alors faites-le nous savoir :
			    		</div>
			    		<div class="suggestion2">
			    			<label>
			    				<input type="radio" name="radio_visage" value="nul" id="nul_checked">
			    				<img 
			    				<?php if ($note_du_site == 1) {
			    					echo "style='border: dashed 4px green; box-shadow: none; border-radius: 50%;'";
			    				};  ?>
			    				alt="icon tête nul" src="img/tete_nul_icon.png" id="nul" onclick="nul_animation(); suivant_animation(this)">
			    			</label>
			    			<label>
			    				<input type="radio" name="radio_visage" value="moyen" id="moyen_checked">
			    				<img 
			    				<?php if ($note_du_site == 2) {
			    					echo "style='border: dashed 4px green; box-shadow: none; border-radius: 50%;'";
			    				};  ?>
			    				alt="icon tête moyen" src="img/tete_moyen_icon.png" id="moyen" onclick="moyen_animation(); suivant_animation(this)">
			    			</label>
			    			<label>
			    				<input type="radio" name="radio_visage" value="bien" id="bien_checked">
			    				<img 
			    				<?php if ($note_du_site == 3) {
			    					echo "style='border: dashed 4px green; box-shadow: none; border-radius: 50%;'";
			    				};  ?>
			    				alt="icon tête bien" src="img/tete_bien_icon.png" id="bien" onclick="bien_animation(); suivant_animation(this)">
			    			</label>			    			
			    		</div>

			    	</div>
			    	<div class="swiper-slide" id="swiper-slide_3">

			    		<div class="question">
			    			Vous avez des idées de modification ?<br>
			    			Faites-le nous savoir :
			    		</div>
			    		<div class="suggestion3">
			    			<div class="guillemet">"</div>
								<textarea 
			    					<?php if ($satisfaction_text) {
			    						echo "style='border: dashed 4px green; box-shadow: none; border-radius: 20px; padding: 10px;'";
			    					}; ?> 
			    					placeholder="Écrire dans cette zone ..." name="say_text" onclick="text_satisfaction_animation(); suivant_animation(this)"><?php if ($satisfaction_text) {
			    						echo($_POST['say_text']);
			    					};  ?></textarea>
			    			<div class="guillemet" style="text-align: right;">"</div>
			    			<label>
				    				<input type="checkbox" name="nothing_to_say" id="nothing_to_say_checkbox">
				    				<span 
				    					<?php if ($satisfaction_no_text) {
				    						echo "style='border: dashed 4px green; box-shadow: none;'";
				    					};  ?> 
				    					class="nothing_to_say" id="nothing_to_say_id" onclick="redire_animation(); suivant_animation(this)">Rien à redire</span>
				    			</label>
			    		</div>
			    	</div>
			    	<div class="swiper-slide" id="swiper-slide_3">

			    		<div class="question">
			    			Comment avez-vous connu notre site ?
			    		</div>

			    		<div class="suggestion4">
				    		<label>
				    			<select size="1" name="visite" class="visite" onclick="suivant_animation(this)">
									<option value="m_d_r">Moteur de recherche</option>
									<option value="reseaux">Réseaux sociaux</option>
									<option value="forum">Sur un forum de discussion</option>
									<option value="site_autre">Lien sur un autre site</option>
									<option value="presse">Presse, brochures touristiques</option>
									<option value="tele">Radio / Télévision</option>
									<option value="bouche_o">Bouche à oreille</option>
						       	</select>
				    		</label>
				    	</div>
			    	</div>
			    	<div class="swiper-slide" id="swiper-slide_3">

			    		<div class="formulaire_submit">
			    			<input type="submit" name="satisfaction_submit" value="- Valider -">
							<div class="formulaire_social">
								<a href="https://fr-fr.facebook.com/paysagedemontagne/" target="_blank"><img alt="facebook icon" class="social" id="facebook" src="img/facebook.png"></a>
								<a href="https://www.instagram.com/Alexstrohl/" target="_blank"><img alt="instagram icon" class="social" id="instagram" src="img/instagram.png"></a>
								<a href="https://twitter.com/search?q=%23montagne" target="_blank"><img alt="twitter icon" class="social" id="twitter" src="img/twitter.png"></a>
								<a href="https://www.pinterest.fr/francemontagnes/nature-en-montagne/" target="_blank"><img alt="pinterest icon" class="social" id="pinterest" src="img/pinterest.png"></a>
							</div>
			    		</div>
			    		
			    	</div>
			    </div>
			    <div 
			    <?php if ($suivant_active) {
			    	echo"style='opacity: 1; z-index: 10; cursor: pointer;'";
			    }
			    else{
			    	echo" onclick='button_prev_algo();'";
			    };  ?>class="prev" id="prev_button">&lt; Retour</div>
			    <div 
			    <?php if ($suivant_active) {
			    	echo"style='opacity: 1; z-index: 10;'";
			    }
			    else{
			    	echo" onclick='hide_next(); button_prev()'";
			    };  ?>
			    class="next" id="next_button">Suivant &gt;</div>
			    <div id="hide"></div>
			</div>
		</div>
	</form>
</div>

<div id="formulaire_alert_container" class="<?php if(isset($_POST['satisfaction_submit'])){
		echo('formulaire_alert');
	}
	else{
		echo('delete');
	} ?>">
	
	<div class="<?php if($validation_message){
			echo('alert_valide'); 
		}
		else{
			echo('delete');			
		}?>">
		<div class="titre_alert">
			Envoi réussi
		</div>
		<div class="validation_message">
			Nous avons bien réceptionné vos réponses.<br>
			Merci pour votre temps et bonne rando.
		</div>
		<div class="button_alert_container">
			<a href=""><div class="alert_button2">Revenir sur la page</div></a>
		</div>
	</div>
</div>