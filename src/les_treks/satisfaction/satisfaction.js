var counter_gr20 = 1;
var counter_way = 1;
var counter_via = 1;
var counter_kerry = 1;
var counter_bernois = 1;
var counter_aucun = 1;

/* ANIMATION BOUTON SUIVANT */

var counter_sug = 1;
var max_counter = 1;

var bouton_suivant_counter = 0;
var bouton_suivant = document.getElementById('next_button');
var hide_animation = document.getElementById('hide');
var validation = 0;


function suivant_animation(elem){

	var id_from_element = elem.id;
	validation = 1;

	bouton_suivant.style.opacity = "1";
	bouton_suivant.style.cursor = "pointer";
	hide_animation.style.display = "none";


	if (id_from_element == "checkbox_aucun") {


		document.getElementById('checkbox_gr20').style.backgroundColor = 'rgba(0,0,0,0)';
		document.getElementById('checkbox_gr20').style.color = 'black';	

		document.getElementById('checkbox_way').style.backgroundColor = 'rgba(0,0,0,0)';
		document.getElementById('checkbox_way').style.color = 'black';

		document.getElementById('checkbox_via').style.backgroundColor = 'rgba(0,0,0,0)';
		document.getElementById('checkbox_via').style.color = 'black';	

		document.getElementById('checkbox_kerry').style.backgroundColor = 'rgba(0,0,0,0)';
		document.getElementById('checkbox_kerry').style.color = 'black';

		document.getElementById('checkbox_bernois').style.backgroundColor = 'rgba(0,0,0,0)';
		document.getElementById('checkbox_bernois').style.color = 'black';

		document.getElementById('checkbox_aucun').style.backgroundColor = 'black';
		document.getElementById('checkbox_aucun').style.color = 'white';
	}
	else{
		document.getElementById('checkbox_aucun').style.backgroundColor = 'rgba(0,0,0,0)';
		document.getElementById('checkbox_aucun').style.color = 'black';		
	};

	bouton_suivant_counter = 0;
};

function hide_next(){
	counter_sug += 1;

	if (counter_sug >= max_counter && validation == 1) {
		validation = 0;
	};

	if (counter_sug >= max_counter){
		max_counter = counter_sug;
	};


	if (validation == 1){
		bouton_suivant.style.opacity = "1";
		bouton_suivant.style.cursor = "pointer";
		hide_animation.style.display = "none";
	}
	else{
		bouton_suivant.style.opacity = "0";
		bouton_suivant.style.cursor = "default";
		hide_animation.style.display = "inline";
	};

	bouton_prev.style.cursor = "pointer";
};

/* BOUTON PRECEDENT ALGO */

var bouton_prev = document.getElementById('prev_button');

function button_prev_algo(){
	counter_sug -=1;

	if (counter_sug < 1){
		counter_sug = 1;
	};

	if (counter_sug == 1) {
		bouton_prev.style.opacity = "0";
		bouton_prev.style.cursor = "default";
	};
	
	if(counter_sug < max_counter){
		validation = 1;
		bouton_suivant.style.opacity = "1";
		bouton_suivant.style.cursor = "pointer";
		hide_animation.style.display = "none";		
	};
};

/* DECLENCHEMENT BOUTON PRECEDENT */

function button_prev(){

	bouton_prev.style.opacity = "1";

};


/* ANIMATION CHECKBOX SUGG1 */


function checkbox_gr20_anim() {
	counter_gr20 += 1;
	document.getElementById('aucun_checkbox').checked = false;

	if (counter_gr20 % 2 == 0){
		document.getElementById('gr20_checkbox').checked = true;
		document.getElementById('checkbox_gr20').style.backgroundColor = 'rgb(84,172,210)';
		document.getElementById('checkbox_gr20').style.color = 'white';
	}
	else{
		document.getElementById('gr20_checkbox').checked = false;
		document.getElementById('checkbox_gr20').style.backgroundColor = 'rgba(0,0,0,0)';
		document.getElementById('checkbox_gr20').style.color = 'black';		
	}
};

function checkbox_way_anim() {
	counter_way += 1;
	document.getElementById('aucun_checkbox').checked = false;

	if (counter_way % 2 == 0){
		document.getElementById('way_checkbox').checked = true;
		document.getElementById('checkbox_way').style.backgroundColor = 'rgb(26,188,156)';
		document.getElementById('checkbox_way').style.color = 'white';
	}
	else{
		document.getElementById('way_checkbox').checked = false;
		document.getElementById('checkbox_way').style.backgroundColor = 'rgba(0,0,0,0)';
		document.getElementById('checkbox_way').style.color = 'black';		
	}
};

function checkbox_via_anim() {
	counter_via += 1;
	document.getElementById('aucun_checkbox').checked = false;

	if (counter_via % 2 == 0){
		document.getElementById('via_checkbox').checked = true;
		document.getElementById('checkbox_via').style.backgroundColor = 'rgb(147,101,184)';
		document.getElementById('checkbox_via').style.color = 'white';
	}
	else{
		document.getElementById('via_checkbox').checked = false;
		document.getElementById('checkbox_via').style.backgroundColor = 'rgba(0,0,0,0)';
		document.getElementById('checkbox_via').style.color = 'black';		
	}
};

function checkbox_kerry_anim() {
	counter_kerry += 1;
	document.getElementById('aucun_checkbox').checked = false;

	if (counter_kerry % 2 == 0){
		document.getElementById('kerry_checkbox').checked = true;
		document.getElementById('checkbox_kerry').style.backgroundColor = 'rgb(184,49,47)';
		document.getElementById('checkbox_kerry').style.color = 'white';
	}
	else{
		document.getElementById('kerry_checkbox').checked = false;
		document.getElementById('checkbox_kerry').style.backgroundColor = 'rgba(0,0,0,0)';
		document.getElementById('checkbox_kerry').style.color = 'black';		
	}
};

function checkbox_bernois_anim() {
	counter_bernois += 1;
	document.getElementById('aucun_checkbox').checked = false;

	if (counter_bernois % 2 == 0){
		document.getElementById('bernois_checkbox').checked = true;
		document.getElementById('checkbox_bernois').style.backgroundColor = 'rgb(251,160,38)';
		document.getElementById('checkbox_bernois').style.color = 'white';
	}
	else{
		document.getElementById('bernois_checkbox').checked = false;
		document.getElementById('checkbox_bernois').style.backgroundColor = 'rgba(0,0,0,0)';
		document.getElementById('checkbox_bernois').style.color = 'black';		
	}
};

function checkbox_aucun_anim() {
	counter_aucun += 1;
	document.getElementById('gr20_checkbox').checked = false;
	document.getElementById('way_checkbox').checked = false;
	document.getElementById('via_checkbox').checked = false;
	document.getElementById('kerry_checkbox').checked = false;
	document.getElementById('bernois_checkbox').checked = false;
	document.getElementById('aucun_checkbox').checked = true;

	if (counter_aucun % 2 == 0){
		document.getElementById('checkbox_aucun').style.backgroundColor = 'black';
		document.getElementById('checkbox_aucun').style.color = 'white';
	}
	else{
		document.getElementById('checkbox_aucun').style.backgroundColor = 'rgba(0,0,0,0)';
		document.getElementById('checkbox_aucun').style.color = 'black';		
	}
};

/* ANIMATION VISAGES */
			    			
var nul = document.getElementById('nul');
var moyen = document.getElementById('moyen');
var bien = document.getElementById('bien');

function nul_animation(){
	document.getElementById('nul_checked').checked = true;
	nul.style.opacity = "1";
	moyen.style.opacity = "0.2";
	bien.style.opacity = "0.2";
};

function moyen_animation(){
	document.getElementById('moyen_checked').checked = true;
	nul.style.opacity = "0.2";
	moyen.style.opacity = "1";
	bien.style.opacity = "0.2";
};

function bien_animation(){
	document.getElementById('bien_checked').checked = true;
	nul.style.opacity = "0.2";
	moyen.style.opacity = "0.2";
	bien.style.opacity = "1";
};


/* ANIMATION CHECKBOX RIEN A REDIRE */

var redire_animation_counter = 1;
var redire_button = document.getElementById('nothing_to_say_id');

function text_satisfaction_animation(){

	document.getElementById('nothing_to_say_checkbox').checked = false;
	redire_button.style.backgroundColor = "rgba(0,0,0,0)";
	redire_button.style.color = "black";

};

function redire_animation(){

	document.getElementById('nothing_to_say_checkbox').checked = true;
	redire_button.style.backgroundColor = "black";
	redire_button.style.color = "white";
};