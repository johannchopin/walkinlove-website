$(function(){

	$('#go_up').click(function() {
		$('html,body').animate({scrollTop: 0}, 'slow');
	});  

	$(window).scroll(function(){
		if($(window).scrollTop() == 0){
			$('#go_up').fadeOut();
		}
		else{
			$('#go_up').fadeIn();
		};
	});

});