<?php

	$trek_name = "West Higland Way";

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<link rel="stylesheet" type="text/css" href="les_treks/way.css">

		<?php include("les_treks/mainpage_head.php"); ?>

		<title>WalkInLove - Nos Treks</title>
	</head>

	<body style="margin: 0; padding: 0;">
		
		<header>
			<?php include("header/header_mini.php");?>			
		</header>

		<main>
			<?php include('go_up/go_up.php');?>

			<div class="map">
				<img alt="localisation W.H.Way" src="img/les_treks_img/way.png">
			</div>

			<div class="way_title">
				<h3>#2<br>- WEST HIGLAND WAY (ÉCOSSE) -</h3>
			</div>

			<div>
				<div class="treks_intro">
					&nbsp;&nbsp;Les splendides paysages d'Écosse font de la randonnée l’un des passe-temps favoris des Écossais comme des touristes. Beaucoup de randonneurs rêvent de parcourir les <span class="important">153 km</span> du West Highland Way, qui relie <span class="milngavie">Milngavie</span> (près de <span class="glasgow">Glasgow</span>) à <span class="fort">Fort William</span>. Cette marche d’une semaine est l’occasion de traverser quelques-uns des plus beaux paysages de la région.
				</div>
			</div>

			<!-- INFOBULLE_TEXTE -->

			<div class="infobulle_container">	
				<div class="infobulle_milngavie">
					<img alt="photo de Milngavie" src="img/les_treks_img/milngavie.png">
					<div>
						<div class="infobulle_text">
							&nbsp;Milngavie est connue comme le début du sentier de grande randonnée "West Highland Way" qui s'étend vers le nord sur 95 miles (153 km) à la ville de Fort William . Un obélisque en granit dans le centre-ville marque le point de départ officiel du sentier.
						</div>
					</div>
				</div>
				<div class="infobulle_glasgow">
					<img alt="vue sur Glasgow" src="img/les_treks_img/glasgow.png">
					<div>
						<div class="infobulle_text">
							&nbsp;Glasgow est la ville la plus grande et la plus peuplée d’Écosse et la troisième ville du Royaume-Uni.
						</div>
					</div>
				</div>
				<div class="infobulle_fort">
					<img alt="photo d'un fort des Higlands" src="img/les_treks_img/fort.png">
					<div>
						<div class="infobulle_text">
							&nbsp;Fort William est située dans les Highlands, au pied du Ben Nevis (sommet le plus haut du Royaume-Uni avec 1 344 mètres), au bord de la faille géologique de Great Glen.
						</div>
					</div>
				</div>
			</div>

			<!-- FIN_INFOBULLE_TEXTE -->

			<div class="carrousel_container">
				<div style="margin-bottom: 20px;">
					<div style="width: 50px;border-bottom: solid white 2px; margin: auto;"></div>
				</div>
				<div class="swiper-container_2">
				    <div class="swiper-wrapper">
				    	<div class="swiper-slide" id="swiper-slide_2" style="background-image:url(img/way_carrousel_1.png)"></div>
				    	<div class="swiper-slide" id="swiper-slide_2" style="background-image:url(img/way_carrousel_2.png)"></div>
				    	<div class="swiper-slide" id="swiper-slide_2" style="background-image:url(img/way_carrousel_3.png)"></div>
				    </div>
				    <!-- Add Pagination -->
				    <div class="swiper-pagination swiper-pagination-white"></div>
				    <div class="swiper-button-next"></div>
				    <div class="swiper-button-prev"></div>
				</div>
				<div style="margin-top: 20px;">
					<div style="width: 50px;border-bottom: solid white 2px; margin: auto;"></div>
				</div>
			</div>


			<div class="titre_section">
				<div style="display: flex; width: auto;">
					<img alt="icon 'lieu'" src="img/lieu_icon.png" style="width: 50px; height: 60px;">
					<div style="display: flex; align-items: center;">
						<div>
							Lieu de départ
						</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
				<div class="destination">
					Glasgow
				</div>
			</div>

			<div style="display: flex;">
				<div class="bordure">
					<img alt="icon d'avion" src="img/avion_icon.png">
					<div class="delimitation_avion"></div>
				</div>
				<div class="trajet_contain">
					<iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d4902441.220205015!2d-8.501642089027925!3d53.13227192813469!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x47e66e1f06e2b70f%3A0x40b82c3688c9460!2sParis!3m2!1d48.856614!2d2.3522219!4m5!1s0x488815562056ceeb%3A0x71e683b805ef511e!2sGlasgow%2C+Royaume-Uni!3m2!1d55.864236999999996!2d-4.251806!5e0!3m2!1sfr!2sfr!4v1513534280211" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					<div>
						<a href="https://www.airfrance.fr" target="blanc"><img alt="airfrance logo" src="img/airfrance_logo.png"></a>
						<a href="http://www.volotea.com/fr" target="blanc"><img alt="volotea logo" src="img/volotea_logo.png"></a>
						<a href="http://www.liligo.fr" target="blanc"><img alt="liligo logo" src="img/liligo_logo.png"></a>
						<a href="https://www.easyvoyage.com" target="blanc"><img alt="easyvoyage logo" src="img/easyvoyage_logo.png"></a>
					</div>
				</div>
			</div>

			<div class="titre_section">
				<div style="display: flex; margin: auto;">
					<img alt="icon séjour" src="img/sejour_icon.png">
					<div>
						<div>Déroulement du Trek</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
			</div>

			<div style="width: 100%; display: flex;">	
				<div class="bordure">
					<img alt="icon sac à dos" src="img/voyage_icon.png">
					<div class="delimitation_sejour"></div>
				</div>
				<div style="margin: auto; display: flex;" class="table">
					<table cellspacing="30" cellpadding="5">
						<thead>
							<tr>
								<th>Etapes</th>
								<th>Description</th>
								<th>Longueur</th>
								<th>Durée</th>
								<th>Niveau</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>#1</td>
								<td>Milngavie - Drymen</td>
								<td>19 km</td>
								<td>5 - 6 heures</td>
								<td>**</td>
							</tr>
							<tr>
								<td>#2</td>
								<td>Drymen - Rowardennan</td>
								<td>23 km</td>
								<td>6 - 7 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#3</td>
								<td>Rowardennan - Inverarnan</td>
								<td>22 km</td>
								<td>6 - 7 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#4</td>
								<td>Inverarnan - Tyndrum</td>
								<td>19 km</td>
								<td>5 - 6 heures</td>
								<td>****</td>
							</tr>
							<tr>
								<td>#5</td>
								<td>Tyndrum - Inveroran</td>
								<td>14 km</td>
								<td>4 - 5 heures</td>
								<td>*</td>
							</tr>
							<tr>
								<td>#6</td>
								<td>Inveroran - Kings House</td>
								<td>15.5 km</td>
								<td>4 - 5 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#7</td>
								<td>Kings House - Kinlochleven</td>
								<td>14 km</td>
								<td>4 - 5 heures</td>
								<td>*****</td>
							</tr>
							<tr>
								<td>#8</td>
								<td>Kinlochleven - Fort William</td>
								<td>24 km</td>
								<td>6 - 7 heures</td>
								<td>**</td>
							</tr>
						</tbody>
					</table>
					<div style="display: flex;">
						<img alt="marche W.H.Way map" src="img/les_treks_img/way_road.png">
					</div>
					</div>
			</div>

			<div class="titre_section">
				<div style="display: flex; margin: auto;">
					<img alt="icon carte" src="img/information2_icon.png" style="width: 50px; height: 50px;">
					<div>
						<div>Pour des informations supplémentaires</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
			</div>

			<div style="width: 100%; display: flex;">
				<div class="bordure">
					<img alt="icon information" src="img/information_icon.png">
					<div class="delimitation_information"></div>
				</div>
				<div class="information_contain">
					<div class="carte_adresse">
						Office de Tourisme de Glasgow<br>
						5 George Square<br>
						Glasgow G2 1DY<br>
						ROYAUME-UNI
						<div style="display: flex;">
							<a href="tel:+441416199001">
								<div class="telephone">
									+44 141 619 9001
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fglasgowloveschristmas%2F&width=149&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId" width="149" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/GlasgowCC?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @GlasgowCC</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
					<div class="carte_adresse">
						ICentre ViitScotland<br>
						Domestic Arrivals Hall<br>
						Paisley PA3 2ST<br>
						ROYAUME-UNI
						<div style="display: flex;">
							<a href="tel:+441413759266">
								<div class="telephone">
									+44 141 375 9266
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fglasgowloveschristmas%2F&width=149&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId" width="149" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/GlasgowCC?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @GlasgowCC</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
					<div class="carte_adresse">
						Glasgow Marriott Hotel<br>
						500 Argyle St<br>
						Glasgow G3 8RR<br>
						ROYAUME-UNI
						<div style="display: flex;">
							<a href="tel:+441412265577">
								<div class="telephone">
									+44 141 226 5577
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fglasgowloveschristmas%2F&width=149&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId" width="149" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/GlasgowCC?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @GlasgowCC</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
				</div>
			</div>

			<div>
				<?php
					include("les_treks/satisfaction/satisfaction.php");
				?>
			</div>
			
		</main>

		<footer>
			<?php include("footer/footer.php");?>
		</footer>

	<?php include('les_treks/mainpage_js.php'); ?>	
		
	</body>
</html>