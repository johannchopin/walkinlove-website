<?php

	$trek_name = "Le GR20";

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">

		<link rel="stylesheet" type="text/css" href="les_treks/gr20.css">


		<?php include("les_treks/mainpage_head.php"); ?>
		<title>WalkInLove - Nos Treks</title>
	</head>
	<body style="margin: 0; padding: 0;">
		

		<header>
			<?php include("header/header_mini.php");?>			
		</header>

		<main>
			<?php include('go_up/go_up.php');?>

			<div class="map">
				<img alt="localisation gr20" src="img/les_treks_img/gr20.png">
			</div>
			<div class="gr20_title">
				<h3>#1<br>- LE GR20 EN CORSE (FRANCE) -</h3>
			</div>

			<div>
				<div class="treks_intro">
					&nbsp;&nbsp;Petit passage par la Corse pour ce trek d’exception. Ouvert en 1972, le GR20 relie <span class="calenzana">Calenzana</span> (au Nord) à <span class="conca">Conca</span> (au Sud) représentant une longueur totale de <span class="important">185 km</span> environs. Réputé comme exigeant, ce mythique sentier de randonnée regorge de <span class="paysage_gr20">paysages époustouflants</span> et de trésors, dont de nombreux lacs, cascades et piscines naturelles.
				</div>
			</div>

			<!-- INFOBULLE_TEXTE -->

			<div class="infobulle_container">
				<div class="infobulle_calenzana">
					<img alt="vue sur Calenzana" src="img/les_treks_img/calenzana.png">
					<div>
						<div class="infobulle_text">
							&nbsp;Calenzana est une commune française du département de la Haute-Corse, dans la collectivité territoriale de Corse.
						</div>
					</div>
				</div>
				<div class="infobulle_conca">
					<img alt="vue sur Conca" src="img/les_treks_img/conca.png">
					<div>
						<div class="infobulle_text">
							&nbsp;Petit village du sud de la Corse niché au creux d'une vallée, Conca constitue la partie finale historique du GR20.
						</div>
					</div>
				</div>
				<div class="infobulle_paysage_gr20">
					<img alt="photo du Monte Cinto" src="img/les_treks_img/paysage_gr20.png">
					<div>
						<div class="infobulle_text">
							&nbsp;Point culminant de l'Ile de Beauté à 2706m, le Monte Cinto fascine. Il fait partie des paysages emblématiques du GR20.
						</div>
					</div>
				</div>
			</div>

			<!-- FIN_INFOBULLE_TEXTE -->
			
			
			<div class="carrousel_container">
				<div style="margin-bottom: 20px;">
					<div style="width: 50px;border-bottom: solid white 2px; margin: auto;"></div>
				</div>
				<div class="swiper-container_2">
				    <div class="swiper-wrapper">
				    	<div class="swiper-slide" id="swiper-slide_2" style="background-image:url(img/gr20_carrousel_1.png)"></div>
				    	<div class="swiper-slide" id="swiper-slide_2" style="background-image:url(img/gr20_carrousel_2.png)"></div>
				    </div>
				    <!-- Add Pagination -->
				    <div class="swiper-pagination swiper-pagination-white"></div>
				    <div class="swiper-button-next"></div>
				    <div class="swiper-button-prev"></div>
				</div>
				<div style="margin-top: 20px;">
					<div style="width: 50px;border-bottom: solid white 2px; margin: auto;"></div>
				</div>
			</div>


			<div class="titre_section">
				<div style="display: flex; width: auto;">
					<img alt="icon 'lieu'" src="img/lieu_icon.png" style="width: 50px; height: 60px;">
					<div style="display: flex; align-items: center;">
						<div>
							Lieu de départ
						</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
				<div class="destination">
					Ajaccio
				</div>
			</div>

			<div style="display: flex;">
				<div class="bordure">
					<img alt="icon d'avion" src="img/avion_icon.png">
					<div class="delimitation_avion"></div>
				</div>
				<div class="trajet_contain">
					<iframe src="https://www.google.com/maps/embed?pb=!1m28!1m12!1m3!1d5751255.359254405!2d1.0390406033564208!3d45.26335811071517!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m13!3e6!4m5!1s0x47e66e1f06e2b70f%3A0x40b82c3688c9460!2sParis!3m2!1d48.856614!2d2.3522219!4m5!1s0x12da69b3d29d2383%3A0x40819a5fd955f00!2sAjaccio!3m2!1d41.919229!2d8.738635!5e0!3m2!1sfr!2sfr!4v1515077331053" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					<div>
						<a href="https://www.airfrance.fr" target="blanc"><img alt="airfrance logo" src="img/airfrance_logo.png"></a>
						<a href="http://www.volotea.com/fr" target="blanc"><img alt="volotea logo" src="img/volotea_logo.png"></a>
						<a href="http://www.liligo.fr" target="blanc"><img alt="liligo icon" src="img/liligo_logo.png"></a>
						<a href="https://www.easyvoyage.com" target="blanc"><img alt="easyvoyage logo" src="img/easyvoyage_logo.png"></a>
					</div>
				</div>
			</div>

			<div class="titre_section">
				<div style="display: flex; margin: auto;">
					<img alt="icon séjour" src="img/sejour_icon.png">
					<div>
						<div>Déroulement du Trek</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
			</div>

			<div style="width: 100%; display: flex;">	
				<div class="bordure">
					<img alt="icon sac à dos" src="img/voyage_icon.png">
					<div class="delimitation_sejour"></div>
				</div>
				<div style="margin: auto; display: flex;" class="table">
					<table cellspacing="30" cellpadding="5">
						<thead>
							<tr>
								<th>Etapes</th>
								<th>Description</th>
								<th>Longueur</th>
								<th>Durée</th>
								<th>Niveau</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>#1</td>
								<td>Calenzana - Ortu di u Piobbu</td>
								<td>10.5 km</td>
								<td>7 - 8 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#2</td>
								<td>Ortu di u Piobbu - Carrozzu</td>
								<td>6.5 km</td>
								<td>6 - 7 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#3</td>
								<td>Carrozzu - Tighjettu</td>
								<td>8 km</td>
								<td>7 - 8 heures</td>
								<td>****</td>
							</tr>
							<tr>
								<td>#4</td>
								<td>Tighjettu - Petra Piana</td>
								<td>14 km</td>
								<td>9 - 11 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#5</td>
								<td>Petra Piana - L'Onda</td>
								<td>10 km</td>
								<td>4 - 5 heures</td>
								<td>**</td>
							</tr>
							<tr>
								<td>#6</td>
								<td>L'Onda - Vizzavona</td>
								<td>10 km</td>
								<td>5 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#7</td>
								<td>Vizzavona - Capannelle</td>
								<td>14 km</td>
								<td>9 - 10 heures</td>
								<td>*****</td>
							</tr>
							<tr>
								<td>#8</td>
								<td>Capannelle - Prati</td>
								<td>17 km</td>
								<td>6 heures</td>
								<td>**</td>
							</tr>
							<tr>
								<td>#9</td>
								<td>Prati - Usciolu</td>
								<td>10.5 km</td>
								<td>6 - 7 heures</td>
								<td>***</td>
							</tr>
							<tr>
								<td>#10</td>
								<td>Usciolu - Matalza</td>
								<td>10.5 km</td>
								<td>6 heures</td>
								<td>**</td>
							</tr>
							<tr>
								<td>#11</td>
								<td>Matalza - Conca</td>
								<td>12 km</td>
								<td>6 - 7 heures</td>
								<td>***</td>
							</tr>
						</tbody>
					</table>
					<div style="display: flex;">
						<img alt="march GR20 map" src="img/les_treks_img/gr20_road.png">
					</div>
					</div>
			</div>

			<div class="titre_section">
				<div style="display: flex; margin: auto;">
					<img alt="icon carte" src="img/information2_icon.png" style="width: 50px; height: 50px;">
					<div>
						<div>Pour des informations supplémentaires</div>
					</div>
				</div>
				<img alt="flèche down" src="img/down.png" style="display: inline; width: 20px; height: 20px;">
			</div>

			<div style="width: 100%; display: flex;">
				<div class="bordure">
					<img alt="icon information" src="img/information_icon.png">
					<div class="delimitation_information"></div>
				</div>
				<div class="information_contain">
					<div class="carte_adresse">
						La Maison du GR20<br>
						2 Piazza Rappalli<br>
						20214 Calenzana<br>
						FRANCE
						<div style="display: flex;">
							<a href="tel:+441416199001">
								<div class="telephone">
									06 27 26 71 83
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Finfogr20corse%2F&width=149&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId" width="149" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/legr20?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @legr20</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
					<div class="carte_adresse">
						Ajaccio office de tourisme<br>
						Porticcio<br>
						20166 Porticcio<br>
						FRANCE
						<div style="display: flex;">
							<a href="tel:+441413759266">
								<div class="telephone">
									04 95 25 10 09
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Finfogr20corse%2F&width=149&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId" width="149" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/legr20?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @legr20</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
					<div class="carte_adresse">
						Office de Tourisme de Piana<br>
						Place de la Mairie<br>
						20115 Piana<br>
						FRANCE
						<div style="display: flex;">
							<a href="tel:+441412265577">
								<div class="telephone">
									09 66 92 84 42
								</div>
							</a>
						</div>
						<div class="social_button">
							<iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Finfogr20corse%2F&width=149&layout=button_count&action=like&size=small&show_faces=false&share=true&height=46&appId" width="149" height="46" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
							<a href="https://twitter.com/legr20?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="false">Follow @legr20</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
					</div>
				</div>
			</div>

			<div>
				<?php
					include("les_treks/satisfaction/satisfaction.php");
				?>
			</div>

		</main>

		<footer>
			<?php include("footer/footer.php");?>
		</footer>

	<?php include('les_treks/mainpage_js.php'); ?>	
		
	</body>
</html>